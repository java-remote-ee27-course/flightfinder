package com.lunamezzogiorno.flightfinder;

import com.lunamezzogiorno.flightfinder.entity.Document;
import com.lunamezzogiorno.flightfinder.entity.Role;
import com.lunamezzogiorno.flightfinder.entity.Traveler;
import com.lunamezzogiorno.flightfinder.entity.User;
import com.lunamezzogiorno.flightfinder.repository.DocumentRepository;
import com.lunamezzogiorno.flightfinder.repository.RoleRepository;
import com.lunamezzogiorno.flightfinder.repository.TravelerRepository;
import com.lunamezzogiorno.flightfinder.repository.UserRepository;
import com.lunamezzogiorno.flightfinder.service.TravelerService;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class DbInitializer implements CommandLineRunner {
    PasswordEncoder passwordEncoder;
    RoleRepository roleRepository;
    UserRepository userRepository;
    TravelerRepository travelerRepository;
    DocumentRepository documentRepository;
    TravelerService travelerService;

    @Override
    public void run(String... args) throws Exception {
        roleRepository.saveAll(createRoles());
        userRepository.saveAll(createUsers());
        documentRepository.saveAll(createDocuments());
        travelerRepository.saveAll(createTravelers());
        //System.out.println("ALL TRAVELERS AND DOCUMENTS: " + travelerRepository.findAll());

        Role userRoleToUpdate = roleRepository.findByAuthority("USER").orElse(null);
        Role adminRoleToUpdate = roleRepository.findByAuthority("ADMIN").orElse(null);

        User commonUserToUpdate = userRepository.findByUsername("user").orElse(null);
        User adminUserToUpdate = userRepository.findByUsername("admin").orElse(null);

        commonUserToUpdate.setAuthorities(Set.of(userRoleToUpdate));
        adminUserToUpdate.setAuthorities(Set.of(adminRoleToUpdate));

        userRepository.saveAll(List.of(commonUserToUpdate, adminUserToUpdate));

        Document passportOfTraveler1 = documentRepository.findById(1L).orElse(new Document(
                "passport",
                "1234",
                LocalDate.of(2024, 11, 23)
        ));
        Document idCardOfTraveler1 = documentRepository.findById(2L).orElse(new Document(
                "id_card",
                "4321",
                LocalDate.of(2024, 11, 23)
        ));
        Traveler traveler1ToUpdate = travelerRepository.findById(1L).orElse(new Traveler(
                "Mari", "Liis", "20-11-2020"
        ));

        traveler1ToUpdate.setDocuments(Set.of(passportOfTraveler1, idCardOfTraveler1));

        passportOfTraveler1.setTraveler(traveler1ToUpdate);
        idCardOfTraveler1.setTraveler(traveler1ToUpdate);

        travelerRepository.saveAll(List.of(traveler1ToUpdate));

    }
    private List<Role> createRoles(){
        Role roleUser = new Role("USER");
        Role roleAdmin = new Role("ADMIN");
        return List.of(roleUser, roleAdmin);
    }
    private List<User> createUsers(){
        User userUser1 = new User("user", passwordEncoder.encode("user"));
        User userAdmin1 = new User("admin", passwordEncoder.encode("admin"));
        return List.of(userUser1, userAdmin1);
    }
    private List<Traveler> createTravelers(){
        Traveler traveler1 = new Traveler("Mari", "Liis", "20-11-2020");
        Traveler traveler2 = new Traveler("Matthew", "Morgan", "20-11-2010");
        return List.of(traveler1, traveler2);
    }
    private Set<Document> createDocuments(){
        Document doc1 = new Document("passport", "1234", LocalDate.of(2024, 11, 23));
        Document doc2 = new Document("id_card", "4321", LocalDate.of(2024, 11, 23));
        Set<Document> docs = new HashSet<>();
        docs.add(doc1);
        docs.add(doc2);
        return docs;
    }

}
