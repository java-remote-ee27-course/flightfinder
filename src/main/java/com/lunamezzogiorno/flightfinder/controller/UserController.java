package com.lunamezzogiorno.flightfinder.controller;

import com.lunamezzogiorno.flightfinder.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@AllArgsConstructor
@RequestMapping("/api/users")
public class UserController {

    UserRepository userRepository;


    @GetMapping(value={"/", ""})
    public String helloUserController(){
        return "User access level";
    }
}

