package com.lunamezzogiorno.flightfinder.controller;

import com.lunamezzogiorno.flightfinder.entity.TravelerDTO;
import com.lunamezzogiorno.flightfinder.service.TravelerDocumentService;
import com.lunamezzogiorno.flightfinder.service.TravelerNameService;
import com.lunamezzogiorno.flightfinder.service.TravelerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
public class TravelerController extends BaseController {
    TravelerService travelerService;
    TravelerNameService travelerNameService;
    TravelerDocumentService travelerDocumentService;

    @PostMapping("travelers")
    public TravelerDTO saveTraveler(@RequestBody TravelerDTO travelerDTO){
        return travelerService.createTraveler(travelerDTO);
    }

    @GetMapping("travelers")
    public List<TravelerDTO> getTravelers(){
        return travelerService.getTravelers();
    }


    @GetMapping("travelers/{travelerId}")
    public TravelerDTO getTraveler(@PathVariable("travelerId") Long id){
        return travelerService.getTraveler(id);
    }

    @PutMapping( "travelers")
    public TravelerDTO updateTraveler(@RequestBody TravelerDTO travelerDTO){
        return travelerService.updateTraveler(travelerDTO);
    }

    @PatchMapping("travelers/name")
    public TravelerDTO updateTravelerName(@RequestBody TravelerDTO travelerDTO){
        return travelerNameService.updateTravelerName(travelerDTO);
    }

    @PatchMapping("travelers/documents")
    public TravelerDTO updateTravelerDocuments(@RequestBody TravelerDTO travelerDTO){
        return travelerDocumentService.updateTravelerDocuments(travelerDTO);
    }

    @PatchMapping("travelers/{travelerId}/documents/{documentNumber}")
    public TravelerDTO updateTravelerDocuments(@PathVariable String documentNumber, @PathVariable Long travelerId){
        return travelerDocumentService.updateTravelerDocuments(travelerId, documentNumber);
    }

    @DeleteMapping("travelers/{travelerId}")
    public ResponseEntity<Long> deleteTraveler(@PathVariable("travelerId") Long id){
        travelerService.deleteTraveler(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
