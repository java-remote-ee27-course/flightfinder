package com.lunamezzogiorno.flightfinder.controller;

import com.lunamezzogiorno.flightfinder.entity.DocumentDTO;
import com.lunamezzogiorno.flightfinder.service.DocumentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
public class DocumentController extends BaseController{
    private DocumentService documentService;

    @GetMapping("/documents/{documentId}")
    public DocumentDTO getDocument(@PathVariable("documentId") Long id){
        return documentService.getDocument(id);
    }

    @PostMapping("/documents")
    public DocumentDTO createDocument(@RequestBody DocumentDTO documentDTO){
        return documentService.createDocument(documentDTO);
    }

    @PutMapping("/documents")
    public DocumentDTO updateDocument(@RequestBody DocumentDTO documentDTO){
        System.out.println("DOCUMENT " + documentDTO);
        return documentService.updateDocument(documentDTO);
    }

    @DeleteMapping("/documents/{documentId}")
    public ResponseEntity<Long> deleteDocument(@PathVariable("documentId") Long id) {
        documentService.deleteDocument(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
