package com.lunamezzogiorno.flightfinder.controller;

import com.lunamezzogiorno.flightfinder.entity.LoginDTO;
import com.lunamezzogiorno.flightfinder.entity.LoginResponseDTO;
import com.lunamezzogiorno.flightfinder.entity.RegistrationDTO;
import com.lunamezzogiorno.flightfinder.entity.User;
import com.lunamezzogiorno.flightfinder.service.AuthenticationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/api/auth")
@AllArgsConstructor
public class AuthenticationController {

    private AuthenticationService authenticationService;

    //error msg is checked in FE, registration.component.ts
    @PostMapping("/register")
    public User registerUser(@RequestBody RegistrationDTO body){
        return authenticationService.registerUser(body.getUsername(), body.getPassword());
    }


    @PostMapping("/login")
    public LoginResponseDTO loginUser(@RequestBody LoginDTO body){
        try{
            authenticationService.loginUser(body.getUsername(), body.getPassword());
        } catch(Exception e) {
            e.printStackTrace();
        }
        return authenticationService.loginUser(body.getUsername(), body.getPassword());
    }
}
