package com.lunamezzogiorno.flightfinder.controller;


import com.amadeus.exceptions.ResponseException;
import com.amadeus.resources.FlightOfferSearch;
import com.amadeus.resources.FlightPrice;
import com.amadeus.resources.Location;
import com.lunamezzogiorno.flightfinder.configuration.AmadeusConnect;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@AllArgsConstructor
public class FlightSearchController extends BaseController {

    private AmadeusConnect amadeusConnect;

    @GetMapping( "/")
    public List<String> getFlights(){
        List<String> flights = new ArrayList<>();
        flights.add("Flight1");
        return flights;
    }

    /**
     * @param keyword=ROM
     * http://localhost:8081/api/locations?keyword=LON
     */
    @GetMapping("/locations")
    public Location[] locations(@RequestParam String keyword) throws ResponseException {
        return amadeusConnect.location(keyword);
    }

    /**
     * @param origin=LON
     * @param destination=SYD
     * @param departDate=2024-11-15
     * @param adults=3
     * @param returnDate=2024-11-20
     * http://localhost:8081/api/flights?origin=LON&destination=NYC&departDate=2024-05-10&returnDate=2024-05-20&adults=3
     */
    @GetMapping("/flights")
    public FlightOfferSearch[] flights( @RequestParam String origin,
                                        @RequestParam String destination,
                                        @RequestParam String departDate,
                                        @RequestParam String adults,
                                        @RequestParam(required = false) String returnDate
                                        )
                                        throws ResponseException {
        return amadeusConnect.flights(origin, destination, departDate, adults, returnDate);
    }


    /**
     * @param search @RequestBody
     */


    @PostMapping("/confirm")
    public FlightPrice flightPrice(@RequestBody() FlightOfferSearch search) throws ResponseException {
        return amadeusConnect.confirm(search);
    }

}
