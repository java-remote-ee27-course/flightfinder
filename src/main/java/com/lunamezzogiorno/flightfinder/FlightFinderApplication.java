package com.lunamezzogiorno.flightfinder;

import com.amadeus.Amadeus;
import com.amadeus.Params;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.referencedata.Locations;
import com.amadeus.resources.FlightOfferSearch;
import com.amadeus.resources.Location;
import com.lunamezzogiorno.flightfinder.configuration.ConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;

import static java.time.LocalTime.now;

@SpringBootApplication
@EnableConfigurationProperties(ConfigProperties.class)
public class FlightFinderApplication {

    private static ConfigProperties configProperties;

    public FlightFinderApplication(ConfigProperties configProperties) {
        this.configProperties = configProperties;
    }

    public static void main(String[] args) throws ResponseException {
        SpringApplication.run(FlightFinderApplication.class, args);


        Amadeus amadeus = Amadeus
                .builder(configProperties.getApiKey(), configProperties.getApiSecret())
                .build();

        /* Next is for demo/testing purpose right now, to be removed later: */
        try{
            Location[] locations = amadeus.referenceData.locations.get(Params
                    .with("keyword", "LON")
                    .and("subType", Locations.ANY));
            System.out.println(Arrays.toString(locations));

        }   catch(Exception e) {
            System.out.println(e +  "Amadeus service not available. " +
                                    "Exception message from PSVM method " +
                                    FlightFinderApplication.class);
        }

        FlightOfferSearch[] flights = amadeus.shopping.flightOffersSearch.get(Params
                .with("originLocationCode", "LON")
                .and("destinationLocationCode", "NYC")
                .and("departureDate", getTodaysDateAndAddDays(15))
                .and("returnDate", getTodaysDateAndAddDays(20))
                .and("adults", 1)
                .and("max", 3));

        System.out.println(Arrays.toString(flights));
    }

    //Create a string of date either today (if argument is 0) or after N days (if argument is N)
    public static String getTodaysDateAndAddDays(long daysToAdd){
        Date date = new Date();
        Instant currDate = date.toInstant();
        Instant firstDate = currDate.plus(daysToAdd, ChronoUnit.DAYS);

        String dateToString = firstDate.toString();
        return dateToString.split("T")[0];
    }
}
