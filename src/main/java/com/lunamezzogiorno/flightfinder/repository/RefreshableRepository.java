package com.lunamezzogiorno.flightfinder.repository;

import org.springframework.stereotype.Repository;

@Repository
public interface RefreshableRepository<T, ID> {
    void refresh(T t);
}
