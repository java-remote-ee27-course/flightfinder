package com.lunamezzogiorno.flightfinder.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

@Repository
public class RefreshableRepositoryImpl<T, ID> implements RefreshableRepository<T, ID>{

    @PersistenceContext
    private EntityManager entityManager;

    public RefreshableRepositoryImpl(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void refresh(T t) {
        entityManager.refresh(t);
    }
}
