package com.lunamezzogiorno.flightfinder.repository;

import com.lunamezzogiorno.flightfinder.entity.Traveler;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TravelerRepository extends JpaRepository<Traveler, Long>, RefreshableRepository<Traveler, Long>  {

}
