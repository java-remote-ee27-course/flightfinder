package com.lunamezzogiorno.flightfinder.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *  UnauthorizedEntryPointHandler implements the BasicAuthenticationEntryPoint interface.
 *  The interface commence() method is used to handle authentication errors,
 *  handles unauthorized access requests by setting the appropriate HTTP status code (401),
 *  sending WWW-Authenticate header,
 *  providing a JSON response with error details to the client,
 *  and creates a new HashMap object to store the error response data.
 *  uses the Jackson ObjectMapper to serialize the resp map into JSON format
 *  and sends it to output stream
 */

@Component
public class UnauthorizedEntryPointHandler implements AuthenticationEntryPoint {

    @Override
    public void commence(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final AuthenticationException authException
    ) throws IOException {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json");
        Map<String, Object> resp = new HashMap<>();
        resp.put("error", HttpStatus.UNAUTHORIZED.getReasonPhrase());
        resp.put("status", HttpStatus.UNAUTHORIZED.value());
        resp.put("message", authException.getMessage());
        resp.put("timestamp", System.currentTimeMillis());
        resp.put("path", request.getServletPath());
        new ObjectMapper().writeValue(response.getOutputStream(), resp);
    }

}
