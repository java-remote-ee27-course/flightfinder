package com.lunamezzogiorno.flightfinder.mapper;

import com.lunamezzogiorno.flightfinder.entity.Document;
import com.lunamezzogiorno.flightfinder.entity.DocumentDTO;
import com.lunamezzogiorno.flightfinder.entity.GeneralDTO;
import com.lunamezzogiorno.flightfinder.entity.TravelerDTO;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.time.LocalDate;

@Mapper(config=BaseMapper.class, componentModel="spring",
        uses = TravelerMapper.class)
public interface DocumentMapper {

    @Mapping(target = "expiryDate", source = "expiryDate")
    @Mapping(target = "travelerId", source= "traveler.id")
    DocumentDTO convertToDocumentDto(Document document);

    @Mapping(target="id", ignore=true)
    //@Mapping(target="traveler.id", source="travelerId")
    @Mapping(target = "expiryDate", source = "expiryDate")
    Document convertToEntity(DocumentDTO documentDTO);


    @Named("expiryDateAdapter")
    default LocalDate fromStringToLocalDate(String dateString) {
        return LocalDate.parse(dateString);
    }


}
