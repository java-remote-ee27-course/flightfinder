package com.lunamezzogiorno.flightfinder.mapper;

import com.lunamezzogiorno.flightfinder.entity.TravelerDTO;
import com.lunamezzogiorno.flightfinder.entity.Traveler;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config=BaseMapper.class, componentModel = "spring",
        uses = {DocumentMapper.class})
public interface TravelerMapper {
    @Mapping(target="travelerId", ignore=true)
    @Mapping(target="documentsDTO", source="documents")
    TravelerDTO convertToTravelerDto(Traveler traveler);

    @Mapping(target="id", ignore = true)
    @Mapping(target="documents", ignore = true)
    Traveler convertToEntity(TravelerDTO travelerDTO);

    /**
    @Mapping(target="id", ignore = true)
    @Mapping(target="documents", source="documentsDTO")
    Traveler convertToTravelerWithDocuments(TravelerDTO travelerDTO);
    */
}
