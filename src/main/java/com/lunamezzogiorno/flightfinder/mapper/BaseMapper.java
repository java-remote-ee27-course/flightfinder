package com.lunamezzogiorno.flightfinder.mapper;

import com.lunamezzogiorno.flightfinder.entity.BaseDTO;
import com.lunamezzogiorno.flightfinder.entity.BaseEntity;
import com.lunamezzogiorno.flightfinder.entity.GeneralDTO;
import org.mapstruct.Context;
import org.mapstruct.MapperConfig;
import org.mapstruct.Mapping;
import org.mapstruct.MappingInheritanceStrategy;

@MapperConfig(
        mappingInheritanceStrategy = MappingInheritanceStrategy.AUTO_INHERIT_FROM_CONFIG
)
public interface BaseMapper {
    @Mapping(target = "id", expression = "java(generalDTO.getTravelerId())")
    BaseEntity toNewBaseEntity(BaseDTO baseDTO, @Context GeneralDTO generalDTO);

    @Mapping(target = "travelerId", ignore = true)
    BaseDTO toBaseDto(BaseEntity entity);
}
