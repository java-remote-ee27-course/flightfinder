package com.lunamezzogiorno.flightfinder.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@ToString
@AllArgsConstructor
public class Traveler extends Person{
    private String firstName;
    private String lastName;
    private String dateOfBirth;

    @EqualsAndHashCode.Exclude
    @JsonBackReference(value="traveler-documents")
    @OneToMany(mappedBy = "traveler", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Document> documents = new HashSet<>();


    @EqualsAndHashCode.Exclude
    @JsonBackReference(value="travelers-phones")
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable( name = "travelers_to_phones",
                joinColumns = {@JoinColumn(name="traveler_id")},
                inverseJoinColumns = {@JoinColumn(name="phone_id")})
    private Set<Phone> phones;

    @EqualsAndHashCode.Exclude
    @JsonBackReference(value="traveler-user")
    @OneToOne   (cascade = CascadeType.ALL)
    @JoinTable  (name = "traveler_to_user",
                 joinColumns = {@JoinColumn(
                        name = "traveler_id",
                        referencedColumnName = "id")},
                 inverseJoinColumns = { @JoinColumn(
                        name = "user_id",
                        referencedColumnName = "id")})
    private User user;

    public Traveler() {
        super();
    }

    public Traveler(Long id) {
        super(id);
    }


    public Traveler(String dateOfBirth) {
        super();
        this.dateOfBirth = dateOfBirth;
    }

    public Traveler(Long id, String dateOfBirth) {
        super(id);
        this.dateOfBirth = dateOfBirth;
    }

    public Traveler(String firstName, String lastName, String dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }

    public Traveler(String dateOfBirth, User user, Set<Document> documents, Set<Phone> phones) {
        this.dateOfBirth = dateOfBirth;
        this.user = user;
        this.documents = documents;
        this.phones = phones;
    }

    public Traveler(Long id, String dateOfBirth, User user, Set<Document> documents, Set<Phone> phones) {
        super(id);
        this.dateOfBirth = dateOfBirth;
        this.user = user;
        this.documents = documents;
        this.phones = phones;
    }

    public Traveler(String firstName, String lastName, String dateOfBirth, Set<Document> documents) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.documents = documents;
    }

    public Traveler(Long id, String firstName, String lastName, String dateOfBirth, Set<Document> documents) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.documents = documents;
    }

    public Traveler(String firstName, String lastName, String dateOfBirth, Set<Document> documents, Set<Phone> phones) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.documents = documents;
        this.phones = phones;
    }

    public Traveler(Long id, String firstName, String lastName, String dateOfBirth, Set<Document> documents, Set<Phone> phones) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.documents = documents;
        this.phones = phones;
    }
}
