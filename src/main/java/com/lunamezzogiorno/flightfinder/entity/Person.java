package com.lunamezzogiorno.flightfinder.entity;

import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public class Person extends BaseEntity {

    public Person() {
        super();
    }

    public Person(Long id) {
        super(id);
    }

}

