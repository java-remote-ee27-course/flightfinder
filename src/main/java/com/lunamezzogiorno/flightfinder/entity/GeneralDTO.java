package com.lunamezzogiorno.flightfinder.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class GeneralDTO {
    private Long travelerId;
    private List<Phone> phones;
    private List<Document> documents;
}
