package com.lunamezzogiorno.flightfinder.entity;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
public class BaseDTO {
    private Long userId;
    private Long travelerId;
}
