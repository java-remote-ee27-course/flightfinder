package com.lunamezzogiorno.flightfinder.entity;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TravelerDTO extends BaseDTO{
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private List<DocumentDTO> documentsDTO;

}
