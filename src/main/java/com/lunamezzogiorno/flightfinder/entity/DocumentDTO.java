package com.lunamezzogiorno.flightfinder.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.lunamezzogiorno.flightfinder.controller.gson.LocalDateTypeAdapter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import java.time.LocalDate;

/**
 *  TODO:: find out why Documents info is not sent to the database.
 */


@Setter
@Getter
@ToString
@AllArgsConstructor
public class DocumentDTO extends BaseDTO{
    private Long id;
    private String documentType;
    private String documentNumber;

    @Expose
    @SerializedName("expiryDate")
    @JsonAdapter(LocalDateTypeAdapter.class)
    private LocalDate expiryDate;


}
