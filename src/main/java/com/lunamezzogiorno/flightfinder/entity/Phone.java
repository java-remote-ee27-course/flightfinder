package com.lunamezzogiorno.flightfinder.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Phone extends BaseEntity{

    private String phoneNumber;

    @ManyToMany(mappedBy = "phones",
                fetch = FetchType.LAZY)
    @JsonBackReference(value="phones-travelers")
    private Set<Traveler> travelers;


}
