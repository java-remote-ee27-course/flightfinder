package com.lunamezzogiorno.flightfinder.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.lunamezzogiorno.flightfinder.controller.gson.LocalDateTypeAdapter;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Just a demo program, for production it is not a good idea to set unique constraint to the document number,
 * if services include international travelers from different countries.
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = "documentNumber")})
public class Document extends BaseEntity{
    private String documentType;
    private String documentNumber;

    @Expose
    @SerializedName("expiryDate")
    @JsonAdapter(LocalDateTypeAdapter.class)
    private LocalDate expiryDate;

    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "traveler_id")
    @EqualsAndHashCode.Exclude
    @JsonBackReference(value="document-traveler")
    private Traveler traveler;

    public Document(Long id, String documentType, String documentNumber, LocalDate expiryDate) {
        super(id);
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.expiryDate = expiryDate;
    }

    public Document(String documentType, String documentNumber, LocalDate expiryDate) {
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.expiryDate = expiryDate;
    }

}
