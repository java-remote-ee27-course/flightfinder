package com.lunamezzogiorno.flightfinder.utility;

import lombok.experimental.UtilityClass;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * TODO:: To be implemented later for LocalDate calculations
 * Creates a LocalDateTime which is correct only in the UTC Zone
 * and gives the time for a country's zone which is given as an argument countryZoneString
 */

@UtilityClass
public class DateFormatter {
    public static LocalDateTime getNowOfCountryZone(String countryZoneString){
        ZoneId localZone = ZoneId.of(countryZoneString);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(LocalDateTime.now(), ZoneOffset.UTC);
        return zonedDateTime.withZoneSameInstant(localZone).toLocalDateTime();

    }
}
