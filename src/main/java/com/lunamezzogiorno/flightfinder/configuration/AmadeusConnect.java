package com.lunamezzogiorno.flightfinder.configuration;

import com.amadeus.Amadeus;
import com.amadeus.Params;
import com.amadeus.exceptions.ResponseException;
import com.amadeus.referencedata.Locations;
import com.amadeus.resources.FlightOfferSearch;
import com.amadeus.resources.FlightPrice;
import com.amadeus.resources.Location;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * AmadeusConnect.class for Amadeus API
 */
@Configuration
@EnableConfigurationProperties(ConfigProperties.class)
public class AmadeusConnect {

    private Amadeus amadeus;
    private ConfigProperties configProperties;

    public AmadeusConnect(@Qualifier("configProperties") ConfigProperties configProperties) {
        this.configProperties = configProperties;
        this.amadeus = Amadeus
                .builder(configProperties.getApiKey(), configProperties.getApiSecret())
                .build();
    }

    public Location[] location(String keyword) throws ResponseException {
        return amadeus.referenceData.locations.get(Params
                .with("keyword", keyword)
                .and("subType", Locations.AIRPORT));
    }

    public FlightOfferSearch[] flights(String origin, String destination, String departDate, String adults, String returnDate) throws ResponseException {
        return this.amadeus.shopping.flightOffersSearch.get(
                  Params.with("originLocationCode", origin)
                        .and("destinationLocationCode", destination)
                        .and("departureDate", departDate)
                        .and("returnDate", returnDate)
                        .and("adults", adults)
                        .and("max", 3));
    }


    //confirm teh price of offer:
    public FlightPrice confirm(FlightOfferSearch offer) throws ResponseException {
        return amadeus.shopping.flightOffersSearch.pricing.post(offer);
    }


}
