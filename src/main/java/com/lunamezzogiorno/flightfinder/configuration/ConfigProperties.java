package com.lunamezzogiorno.flightfinder.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Data
@Configuration
@ConfigurationProperties(prefix = "flightfinder")
public class ConfigProperties {
    private String apiKey;
    private String apiSecret;

}
