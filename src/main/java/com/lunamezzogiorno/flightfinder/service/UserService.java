package com.lunamezzogiorno.flightfinder.service;

import com.lunamezzogiorno.flightfinder.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService<T> {
    public Optional<User> findUserByUsername(String username);
    public void addUser(User user);
    List<T> getUsers();

}
