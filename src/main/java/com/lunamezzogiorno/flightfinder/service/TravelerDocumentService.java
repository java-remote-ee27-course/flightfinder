package com.lunamezzogiorno.flightfinder.service;

import com.lunamezzogiorno.flightfinder.entity.TravelerDTO;
import org.springframework.stereotype.Service;

//Should be under mapper, instead. Needs to be re-written later inside Mapper instead.
@Service
public interface TravelerDocumentService {
    TravelerDTO updateTravelerDocuments(TravelerDTO travelerDTO);
    TravelerDTO updateTravelerDocuments(Long travelerId, String documentNumber);
}
