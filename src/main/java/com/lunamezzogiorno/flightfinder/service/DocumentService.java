package com.lunamezzogiorno.flightfinder.service;

import com.lunamezzogiorno.flightfinder.entity.Document;
import com.lunamezzogiorno.flightfinder.entity.DocumentDTO;
import com.lunamezzogiorno.flightfinder.entity.Traveler;

import java.util.List;

public interface DocumentService {
    DocumentDTO getDocument(Long documentId);
    Document getDocumentByDocumentNumber(String documentNumber);
    List<DocumentDTO> getDocuments();
    DocumentDTO createDocument(DocumentDTO documentDTO);

    void saveDocuments(Traveler traveler, List<DocumentDTO> documentDTOs);
    DocumentDTO updateDocument(DocumentDTO documentDTO);
    void deleteDocument(Long documentId);
}
