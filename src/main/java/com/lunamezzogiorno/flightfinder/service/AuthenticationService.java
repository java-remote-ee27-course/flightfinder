package com.lunamezzogiorno.flightfinder.service;

import com.lunamezzogiorno.flightfinder.entity.LoginResponseDTO;
import com.lunamezzogiorno.flightfinder.entity.Role;
import com.lunamezzogiorno.flightfinder.entity.User;
import com.lunamezzogiorno.flightfinder.repository.RoleRepository;
import com.lunamezzogiorno.flightfinder.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
@AllArgsConstructor
public class AuthenticationService {
    private PasswordEncoder passwordEncoder;
    private RoleRepository roleRepository;
    private UserRepository userRepository;
    private AuthenticationManager authenticationManager;
    private TokenService tokenService;

    public User registerUser(String username, String password) {
        String encodedPassword = passwordEncoder.encode(password);
        Role userRole = roleRepository.findByAuthority("USER").get();
        Set<Role> roles = new HashSet<>();
        roles.add(userRole);
        return userRepository.save(new User(username, encodedPassword, roles));
    }

    public LoginResponseDTO loginUser(String username, String password){
        System.out.println("Authservice " + username);
        try{
            Authentication auth = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password));
            String token = tokenService.generateJwt(auth);
            return new LoginResponseDTO(userRepository.findByUsername(username).get(), token);

        } catch(AuthenticationException e){
            log.info("Could not login. Problem description: " + e);
            return new LoginResponseDTO(null, "");
        }
    }
}
