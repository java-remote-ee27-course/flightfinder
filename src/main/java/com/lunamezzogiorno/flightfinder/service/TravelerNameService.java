package com.lunamezzogiorno.flightfinder.service;

import com.lunamezzogiorno.flightfinder.entity.TravelerDTO;
import org.springframework.stereotype.Service;

@Service
public interface TravelerNameService {
    TravelerDTO updateTravelerName(TravelerDTO travelerDTO);
}
