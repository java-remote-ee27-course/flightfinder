package com.lunamezzogiorno.flightfinder.service;

import com.lunamezzogiorno.flightfinder.entity.User;
import com.lunamezzogiorno.flightfinder.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service("UserService")
public class UserServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;


    // returned user by next method must be an instance of UserDetails.
    // This is a type used by Spring Security to check
    // if the user is not locked or disabled by administrator etc.
    // so User class or wrapper class needs to implement UserDetails interface
    @Override
    public UserDetails loadUserByUsername(String username) {
        System.out.println("In user service impl");
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("user not valid"));
    }

    public User getAuthenticatedUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String login = auth.getName();
        return userRepository.findByUsername(login).orElse(null);
    }
}
