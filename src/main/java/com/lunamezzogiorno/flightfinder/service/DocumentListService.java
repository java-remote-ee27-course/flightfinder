package com.lunamezzogiorno.flightfinder.service;

import com.lunamezzogiorno.flightfinder.entity.Document;
import com.lunamezzogiorno.flightfinder.entity.DocumentDTO;
import com.lunamezzogiorno.flightfinder.entity.Traveler;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface DocumentListService {
    List<Document> getDocumentsListFromDTO(Traveler traveler, List<DocumentDTO> documentDTOs);
}
