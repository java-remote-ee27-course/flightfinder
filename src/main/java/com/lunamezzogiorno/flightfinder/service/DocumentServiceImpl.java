package com.lunamezzogiorno.flightfinder.service;

import com.lunamezzogiorno.flightfinder.entity.Document;
import com.lunamezzogiorno.flightfinder.entity.DocumentDTO;
import com.lunamezzogiorno.flightfinder.entity.Traveler;
import com.lunamezzogiorno.flightfinder.mapper.DocumentMapper;
import com.lunamezzogiorno.flightfinder.repository.DocumentRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DocumentServiceImpl implements DocumentService, DocumentListService {
    private DocumentRepository documentRepository;
    private DocumentMapper documentMapper;


    @Override
    public DocumentDTO getDocument(Long documentId) {
        Document document = getDocumentById(documentId);
        return documentMapper.convertToDocumentDto(document);
    }


    @Override
    @Transactional
    public DocumentDTO updateDocument(DocumentDTO documentDTO) {
        Document documentFromDatabase = getDocumentById(documentDTO.getId());
        System.out.println("Document id: "+ documentDTO.getId());
        documentFromDatabase.setDocumentType(documentDTO.getDocumentType());
        documentFromDatabase.setDocumentNumber(documentDTO.getDocumentNumber());
        documentFromDatabase.setExpiryDate(documentDTO.getExpiryDate());
        System.out.println("Document before saving: " + documentFromDatabase);
        Document saved = documentRepository.saveAndFlush(documentFromDatabase);
        System.out.println("SAVED");
        return documentMapper.convertToDocumentDto(saved);
    }


    private Document getDocumentById(Long documentId){
        return documentRepository.findById(documentId).orElseThrow(() ->
                new EntityNotFoundException(
                        getClass() +
                                " Document not found by id: " +
                                documentId));
    }

    @Override
    public Document getDocumentByDocumentNumber(String documentNumber) {
        return documentRepository.findByDocumentNumber(documentNumber)
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                getClass() +
                                " Document not found by document number: " +
                                documentNumber));

    }


    @Override
    public List<DocumentDTO> getDocuments() {
        List<Document> documents = documentRepository.findAll();
        return documents.stream().map(doc -> documentMapper.convertToDocumentDto(doc)).toList();
    }

    //saving doc w/o traveler's info:
    @Override
    public DocumentDTO createDocument(DocumentDTO documentDTO) {
        Document document = convertToDocument(null, documentDTO);
        Document saved = documentRepository.saveAndFlush(document);
        return documentMapper.convertToDocumentDto(saved);
    }



    //delete document
    @Override
    @Transactional
    public void deleteDocument(Long documentId) {
        if(documentRepository.existsById(documentId)){
            documentRepository.deleteById(documentId);

        } else{
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    "Document was not found, either it has been deleted or does not exist");
        }
    }

    //Save traveler's documents
    @Override
    @Transactional
    public void saveDocuments(Traveler traveler, List<DocumentDTO> documentDTOs){
        List <Document> docsToBeSaved = getDocumentsListFromDTO(traveler, documentDTOs);
        documentRepository.saveAllAndFlush(docsToBeSaved);
    }

    //With saveDocuments
    public List<Document> getDocumentsListFromDTO(Traveler traveler, List<DocumentDTO> documentDTOs){
        return documentDTOs
                .stream()
                .map(dto -> convertToDocument(traveler, dto))
                .toList();
    }

    //With getDocumentsListFromDTO which is together w. saveDocuments
    //for persistence later, without id
    private Document convertToDocument(Traveler traveler, DocumentDTO documentDTO){
        Document document = documentMapper.convertToEntity(documentDTO);
        if(traveler != null){
            document.setTraveler(traveler);
        }
        return document;
    }
}
