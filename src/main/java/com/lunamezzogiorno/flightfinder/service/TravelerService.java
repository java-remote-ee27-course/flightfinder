package com.lunamezzogiorno.flightfinder.service;


import com.lunamezzogiorno.flightfinder.entity.DocumentDTO;
import com.lunamezzogiorno.flightfinder.entity.TravelerDTO;
import com.lunamezzogiorno.flightfinder.entity.Traveler;

import java.util.List;

public interface TravelerService {
    TravelerDTO getTraveler(Long travelerId);
    Traveler findById(Long travelerId);
    List<TravelerDTO> getTravelers();

    TravelerDTO createTraveler(TravelerDTO travelerDTO);
    TravelerDTO updateTraveler(TravelerDTO travelerDTO);


    void deleteTraveler(Long travelerId);


}
