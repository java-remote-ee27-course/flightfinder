package com.lunamezzogiorno.flightfinder.service;

import com.lunamezzogiorno.flightfinder.entity.Document;
import com.lunamezzogiorno.flightfinder.entity.DocumentDTO;
import com.lunamezzogiorno.flightfinder.entity.TravelerDTO;
import com.lunamezzogiorno.flightfinder.entity.Traveler;
import com.lunamezzogiorno.flightfinder.mapper.TravelerMapper;
import com.lunamezzogiorno.flightfinder.repository.TravelerRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * TravelerServiceImpl.class
 *
 * Transactional, with parameters:
 * https://stackoverflow.com/questions/11746499/how-to-solve-the-failed-to-lazily-initialize-a-collection-of-role-hibernate-ex
 */

@Slf4j
@Service
@AllArgsConstructor
public class TravelerServiceImpl implements TravelerService, TravelerNameService, TravelerDocumentService{
    private TravelerRepository travelerRepository;
    private TravelerMapper travelerMapper;
    private DocumentService documentService;
    private DocumentListService documentListService;


    @Override
    public TravelerDTO getTraveler(Long travelerId){
        Traveler traveler = findById(travelerId);
        return travelerMapper.convertToTravelerDto(traveler);
    }



    @Override
    public List<TravelerDTO> getTravelers() {
        return findAllTravelers()
                .stream()
                .map(traveler -> travelerMapper.convertToTravelerDto(traveler))
                .collect(Collectors.toList());
    }

    /**
     *
     * @param travelerDTO
     * @return
     */
    @Override
    @Transactional
    public TravelerDTO createTraveler(TravelerDTO travelerDTO) {
        Traveler traveler = travelerMapper.convertToEntity(travelerDTO);
        List<DocumentDTO> documentDTOs = travelerDTO.getDocumentsDTO();
        Traveler savedTraveler = saveTraveler(traveler, documentDTOs);
        return travelerMapper.convertToTravelerDto(savedTraveler);
    }

    /**
     * @param travelerDTO
     * @return TravelerDTO
     */
    @Override
    @Transactional
    public TravelerDTO updateTraveler(TravelerDTO travelerDTO) {
        //check if traveler exists
        findById(travelerDTO.getTravelerId());

        Traveler travelerToUpdate = travelerMapper.convertToEntity(travelerDTO);
        travelerToUpdate.setId(travelerDTO.getTravelerId());
        List<DocumentDTO> documents = travelerDTO.getDocumentsDTO();
        Traveler updated = saveTraveler(travelerToUpdate, documents);
        return travelerMapper.convertToTravelerDto(updated);
    }


    /**
     * @param travelerDTO
     * @return TravelerDTO
     */

    @Override
    @Transactional
    public TravelerDTO updateTravelerName(TravelerDTO travelerDTO) {
        Traveler traveler = findById(travelerDTO.getTravelerId());
        traveler.setFirstName(travelerDTO.getFirstName());
        traveler.setLastName(travelerDTO.getLastName());
        Traveler updated = saveTraveler(traveler, null);
        return travelerMapper.convertToTravelerDto(updated);
    }


    /**
     * @param travelerId
     */
    @Override
    @Transactional
    public void deleteTraveler(Long travelerId) {
        if(travelerRepository.existsById(travelerId)){
            travelerRepository.deleteById(travelerId);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Data not found in repository!");
        }
    }

    /**
     * @param travelerId
     * @return Traveler
     */
    @Override
    public Traveler findById(Long travelerId) {
        return travelerRepository.findById(travelerId)
                .orElseThrow(() -> new EntityNotFoundException(getClass() + " Traveler not found by id: " + travelerId));
    }

    private List<Traveler> findAllTravelers(){
        return travelerRepository.findAll();
    }

    private Traveler saveTraveler(Traveler traveler, List<DocumentDTO> documents){
        Traveler saved = travelerRepository.saveAndFlush(traveler);
        addTravelerDocuments(documents, saved);
        travelerRepository.refresh(saved);
        return saved;
    }


    private void addTravelerDocuments(List<DocumentDTO> documents, Traveler traveler){
        if(documents !=null){
            documentService.saveDocuments(traveler, documents);
        }
    }

    //Traveler with updated documents list - add new documents

    /**
     *
     * @param travelerDTO
     * Required is: Traveler with at least their id - travelerId, and list of new documents
     * (as new docs will be added, their id-s are created automatically, thus no docs id-s needed)
     * @return
     * An example:
     * {
     *         "travelerId":1,
     *         "documentsDTO": [
     *             {
     *                 "documentType": "passport",
     *                 "documentNumber": "1234L",
     *                 "expiryDate": "23-11-2024",
     *                 "travelerId": 1
     *             },
     *             {
     *                 "documentType": "id_card",
     *                 "documentNumber": "4325D",
     *                 "expiryDate": "23-11-2024",
     *                 "travelerId": 1
     *             }
     *         ]
     * }
     */
    @Override
    @Transactional
    public TravelerDTO updateTravelerDocuments(TravelerDTO travelerDTO) {
        //get traveler and their documents from database
        Traveler travelerFromDatabase = travelerRepository.findById(travelerDTO.getTravelerId()).orElseThrow();
        Set<Document> travelerDocumentsFromDatabase = travelerFromDatabase.getDocuments();
        //add new documents
        List<Document> docs = documentListService.getDocumentsListFromDTO(travelerFromDatabase, travelerDTO.getDocumentsDTO());
        travelerDocumentsFromDatabase.addAll(docs);
        //replace traveler's old docs with old+new docs
        travelerFromDatabase.setDocuments(travelerDocumentsFromDatabase);
        //save traveler with updated docs:
        Traveler saved = travelerRepository.saveAndFlush(travelerFromDatabase);
        return travelerMapper.convertToTravelerDto(saved);
    }

    /**
     * Add one document to traveler:
     * @param travelerId
     * @param documentNumber
     * @return
     */
    @Override
    @Transactional
    public TravelerDTO updateTravelerDocuments(Long travelerId, String documentNumber){
        Traveler traveler = travelerRepository.findById(travelerId).orElseThrow(() ->
                new EntityNotFoundException(getClass() + " Traveler not found by id: " + travelerId));
        Document document = documentService.getDocumentByDocumentNumber(documentNumber);
        Set<Document> travelerDocumentsFromDatabase = traveler.getDocuments();
        travelerDocumentsFromDatabase.add(document);
        traveler.setDocuments(travelerDocumentsFromDatabase);
        document.setTraveler(traveler);
        Traveler saved = travelerRepository.saveAndFlush(traveler);
        return travelerMapper.convertToTravelerDto(saved);
    }
}
