# FlightFinder App

**An app that uses Amadeus API to retrieve flights information.**

The app is currently under development, so be patient, not everything is working yet. :) 30.03.2024 - ...

## Technologies used

* Spring Boot / Spring Security BE
* Amadeus API
* Rest API
* MySQL database

## Functionalities
* Query flights info from a city / airport, i.e LON (through Amadeus API)
* Query flight offers from one destination to another with dates, passengers number etc. (through Amadeus API) 
* GET travelers data from BE 
* POST travelers data 
* DELETE travelers data
* PUT travelers data
* Initial data seed of 2 travelers are created in `./flightfinder/src/main/java/com/lunamezzogiorno/flightfinder/DbInitializer`

## ToDo

* Finish the backend:
  * Add more Amadeus API functionalities
  * Complete the travelers registration, checks etc. 
  * Add missing entities, repositories, services, controllers etc.) 
  * Change Traveler's birthday field to date field
  * Structure better the Amadeus API part
  * Finish the user rights and pages access part (as right now it is: .requestMatchers("/**").permitAll() )
  * etc.
* Add the frontend

## Prerequisites to run the code
* Java
* Maven 
* Amadeus API key+secret
* Web connection

## Install and run
* You need an Amadeus API key to make flights search queries within this app: https://developers.amadeus.com/get-started/get-started-with-self-service-apis-335
* `git clone  https://gitlab.com/java-remote-ee27-course/flightfinder.git`
* copy the file `src/main/resources/application.properties.example` into `src/main/resources/application.properties` and provide appropriate options
* Run the app from: `./flightfinder/src/main/java/com/lunamezzogiorno/flightfinder/FlightFinderApplication.java`
* Database and tables are generated automatically due to the application.properties settings


    ![main.png](pictures%2Fmain.png)